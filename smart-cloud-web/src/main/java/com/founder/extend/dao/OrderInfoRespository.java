package com.founder.extend.dao;

import com.founder.extend.domain.OrderInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Component
@Repository
public interface OrderInfoRespository extends JpaRepository<OrderInfo, String> {
}
